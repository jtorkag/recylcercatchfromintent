package com.skytribe.duderecycle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

public class MainActivity extends Activity implements OnClickListener {

    public RecyclerView mCompleteRecyclerView;
    private Button mClearList;
    private ArrayList<Song> mSongList;
    private RecyclerView.Adapter mAListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        assignDataToArrayList();
        mAListAdapter = new Adapter(this, mSongList);
        mCompleteRecyclerView.setAdapter(mAListAdapter);
        mCompleteRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mCompleteRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Intent intentReceive = getIntent();
        String action = intentReceive.getAction();
        String type = intentReceive.getType();
        verifyIntent(action, type, intentReceive);
        mAListAdapter.notifyDataSetChanged();


    }

    private void saveSongList(ArrayList<Song> listToSave) {
        String filename = "savedSongs.srl";
        ObjectOutput out = null;

        try {
            out = new ObjectOutputStream(new FileOutputStream(new File(getFilesDir(), "") + File.separator + filename));
            out.writeObject(listToSave);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private ArrayList<Song> loadSongList() {
        ObjectInputStream input;
        ArrayList<Song> loadedSongList = null;
        String filename = "savedSongs.srl";

        try {
            input = new ObjectInputStream(new FileInputStream(new File(new File(getFilesDir(), "") + File.separator + filename)));
            loadedSongList = (ArrayList<Song>) input.readObject();

            input.close();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return loadedSongList;

    }

    private void assignDataToArrayList() {

        if (loadSongList() != null) {
            mSongList = loadSongList();
        } else {
            mSongList = new ArrayList<Song>();
        }
    }

    private void verifyIntent(String action, String type, Intent intentReceive) {
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSentText(intentReceive);
            }
        }
    }

    void handleSentText(Intent intentR) {
        String sharedText = intentR.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            addSongToList(sharedText);
            //try {
            saveSongList(mSongList);
            // }
            // catch(Exception e){

            // }
        }
    }


    private void initViews() {
        mCompleteRecyclerView = (RecyclerView) findViewById(R.id.song_list);
        mClearList = (Button) findViewById(R.id.clearList);
        mClearList.setOnClickListener(this);

    }

    private void addSongToList(String songUrl) {
        //int randomVal = 0 + (int) (Math.random() * ((1000 - 0) + 1));
        mSongList.add(new Song(songUrl));
        mAListAdapter.notifyDataSetChanged();
    }
    

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clearList:
                mSongList.clear();
                mAListAdapter.notifyDataSetChanged();
                saveSongList(mSongList);
                break;
        }

    }

}
