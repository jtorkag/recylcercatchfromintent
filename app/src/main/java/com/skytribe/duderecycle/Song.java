package com.skytribe.duderecycle;


import java.io.Serializable;

public class Song implements Serializable {
    private String songUrl;

    Song(String songUrl) {
        this.songUrl = songUrl;
    }


    public String getSongUrl() {
        return songUrl;
    }

    public void setSongUrl(String newSongUrl) {
        this.songUrl = newSongUrl;
    }


}