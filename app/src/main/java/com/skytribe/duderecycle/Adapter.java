package com.skytribe.duderecycle;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private Activity mContext;
    private ArrayList<Song> mSongList;
    private LayoutInflater mLayoutInflater = null;


    public Adapter(Activity context, ArrayList<Song> songList){

        mContext = context;
        mSongList = songList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getItemCount(){
        return mSongList.size();
    }

    public Object getItem(int pos) {
        return mSongList.get(pos);

    }
    @Override
    public long getItemId(int pos){
        return pos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_container,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Song song = mSongList.get(position);

        holder.songUrl.setText(song.getSongUrl());
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView songUrl;

        public ViewHolder (View songView){
            super (songView);
            songUrl = (TextView) songView.findViewById(R.id.song_url);
        }
    }

}



